FROM alpine

RUN apk add --no-cache python

ENV PORT 8080

WORKDIR /app
COPY ./app .

EXPOSE ${PORT}

CMD python -m  SimpleHTTPServer ${PORT}
